#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QFile>
#include <QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onDeviceSelected(QAction*);
    void onValueChange(int);
    void onShowInfo();

private:
    Ui::MainWindow *ui;
    QLabel *status;
    QFile *tty;

    QStringList getTTYs();
    void buildTTYsMenu();
    QFile* openTTY(const QString&);
};

#endif // MAINWINDOW_H
