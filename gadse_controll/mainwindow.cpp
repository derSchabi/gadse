#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDir>
#include <QFileInfoList>
#include <iostream>
#include <QDebug>
#include <QAction>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    status(new QLabel(this)),
    tty(0)
{
    ui->setupUi(this);

    statusBar()->addWidget(status);
    status->setText("Not connected");

    buildTTYsMenu();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onDeviceSelected(QAction *action)
{
    tty = openTTY(action->text());
    onValueChange(ui->angleDial->value());
}

void MainWindow::onValueChange(int angle)
{
    if(tty != 0)
    {
        tty->write(QString((QString::number(angle) + QString("\n"))).toUtf8());
        tty->flush();
    }
}

void MainWindow::onShowInfo()
{

}

QStringList MainWindow::getTTYs()
{
    QDir dev = QDir("/dev", "tty*", QDir::Name, QDir::System);
    QFileInfoList list = dev.entryInfoList();

    auto possibleTTYs = QStringList();
    for (QFileInfo info : list)
    {
        if(info.baseName().startsWith("ttyUSB") || info.baseName().startsWith("ttyACM"))
        {
            possibleTTYs.append(info.absolutePath()+"/"+info.baseName());
        }
    }

    return possibleTTYs;
}

void MainWindow::buildTTYsMenu()
{
    for(QString device : getTTYs())
    {
        ui->menuDevice->addAction(device);
    }
}

QFile *MainWindow::openTTY(const QString &name)
{
    delete tty;
    QFile *tty = new QFile(name);
    if(!tty->open(QFile::WriteOnly))
    {
        status->setText("Could not open " + tty->fileName());
        delete tty;
    } else {
        status->setText("Connected to " + tty->fileName());
    }

    return tty;
}
