$fn = 40;

difference() {
    translate([0, 0, -7.5]) {
        cube([38, 38, 4.5 + 5.5 + 45.5]);
    }
    translate([3.5 + 4, 3.5+ 1, -20]) {
        translate([0, 0, 0]) cylinder(h=20, r=4);
        translate([0, 28.5, 0]) cylinder(h=20, r=4);
        translate([24.5, 0, 0]) cylinder(h=20, r=4);
        translate([24.5, 28.5, 0]) cylinder(h=20, r=4);
    }
    
    translate([-1, 9-0.25, 0-0.25]) cube([100, 20+0.5, 40.5+0.5]);
    
    translate([10, -1, 5]) cube([100, 100, 100]);
    
    translate([0, 19, 45]) {
        translate([-1, -5, 0]) rotate([90, 0, 90]) cylinder(h=20, r=2);
        translate([-1, 5, 0]) rotate([90, 0, 90]) cylinder(h=20, r=2);
    }
    
    translate([0, 19, -5]) {
        translate([-1, -5, 0]) rotate([90, 0, 90]) cylinder(h=15, r=2);
        translate([-1, 5, 0]) rotate([90, 0, 90]) cylinder(h=15, r=2);
    }
    
    translate([3, 19-7.5, -19]) cube([10,15,20]);
    translate([3, 19-7.5, 40]) cube([10,15,20]);
 }