 #include <Servo.h> //Die Servobibliothek wird aufgerufen. Sie wird benötigt, damit die Ansteuerung des Servos vereinfacht wird.
 #include <FastLED.h>
 
const int SERVO_PIN = 8;
const int LASER_PIN_1 = 10;
const int LASER_PIN_2 = 11;
const int LASER_PIN_3 = 12;
const int LASER_PIN_4 = 13;
const int WS_PIN = 6;

const int TIME_UNTIL_UPDATE = 16; //ms

Servo hand;
CRGB eyes[2];

// externally settable states
int movementAngle = 90;

// movement state
int currentAngle = 0;
int desiredAngle;
unsigned long lastUpdate = 0;
bool moveForward = true;



bool updateRequired() {
    unsigned long mils = millis();
    if(mils >= lastUpdate + TIME_UNTIL_UPDATE) {
        lastUpdate = mils;
        return true;
    }
    return false;
}

void moveServo(int angle) {
  desiredAngle = angle;
}

void updateServo(bool movingForward) {
  if(movingForward) {
    currentAngle++;
  } else {
    currentAngle--;
  }
  hand.write(currentAngle);
}

bool updateCatRequired(bool movingForward) {
  Serial.print(desiredAngle);
  Serial.print("\t");
  Serial.println(currentAngle);
  return desiredAngle == currentAngle;
}


// reads an angle from uart and applies it as moving angle
void readUART() {
  while(Serial.available()) {
    int val = Serial.parseInt();
    if(val > 0) {
      movementAngle = val;
    }
  }
}

void randomLightFoo() {
  eyes[0].setHSV(random(1, 256), 255, 255);
  eyes[1].setHSV(random(1, 256), 255, 255);
  FastLED.show();
  activateLaser(random(1, 16));
}

void activateLaser(int pattern) {
  digitalWrite(LASER_PIN_1, pattern & 1 ? HIGH : LOW);
  digitalWrite(LASER_PIN_2, pattern & 2 ? HIGH : LOW);
  digitalWrite(LASER_PIN_3, pattern & 4 ? HIGH : LOW);
  digitalWrite(LASER_PIN_4, pattern & 8 ? HIGH : LOW);
}


void setup() {
  hand.attach(SERVO_PIN);
  pinMode(LASER_PIN_1, OUTPUT);
  pinMode(LASER_PIN_2, OUTPUT);
  pinMode(LASER_PIN_3, OUTPUT);
  pinMode(LASER_PIN_4, OUTPUT);

  FastLED.addLeds<WS2812, WS_PIN, GRB>(eyes, 2);
  Serial.begin(9600);
  Serial.write("start\n");

  // set init state
  digitalWrite(LASER_PIN_1, LOW);
  digitalWrite(LASER_PIN_2, LOW);
  digitalWrite(LASER_PIN_3, LOW);
  digitalWrite(LASER_PIN_4, LOW);

  eyes[0] = CRGB::Blue;
  eyes[1] = CRGB::Blue;

  FastLED.show();
  
  moveServo(90 + movementAngle/2);
}


void loop()
{
  readUART();
  if(updateRequired()) {
    
    updateServo(moveForward);
    
    if(updateCatRequired(moveForward)) {
      // state transition here
      
      randomLightFoo();

      if(moveForward) {
        moveServo(90 - movementAngle/2);
      } else {
        moveServo(90 + movementAngle/2);
      }
      moveForward = !moveForward;
    }
  }
}
